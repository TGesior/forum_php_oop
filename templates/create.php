<?php include('includes/header.php'); ?>	
<form role="form" method="post" action="create.php">
							<div class="form-group">
								<label>Tytul tematu</label>
								<input type="text" class="form-control" name="title" placeholder="Wpisz tytul tematu">
							</div>
							<div class="form-group">
								<label>Kategorie</label>
								<select class="form-control" name="category">
								<?php foreach(getCategories() as $category) : ?>
									<option value="<?php echo $category->id; ?>"><?php echo $category->name; ?></option>
								<?php endforeach; ?>
							</select>
							</div>
								<div class="form-group">
									<label>Tresc Tematu</label>
									<textarea id="body" rows="10" cols="80" class="form-control" name="body"></textarea>
									<script>CKEDITOR.replace('body');</script>
								</div>
							<button name="do_create" type="submit" class="btn btn-default">Zatwierdz</button>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div id="sidebar">
					<div class="block">
						<h3>Formularz logujacy</h3>
						<form role="form">
						<div class="form-group">
							<label>Nazwa uzytkownika</label>
							<input name="username" type="text" class="form-control" placeholder="Wpisz nazwe uzytkownika">
						</div>
						<div class="form-group">
							<label>Haslo</label>
							<input name="password" type="password" class="form-control" placeholder="Wpisz haslo ">
						</div>			
						<button name="do_login" type="submit" class="btn btn-primary">Zaloguj sie</button> <a  class="btn btn-default" href="register.html"> Stworz konto</a>
					</form>
<?php include('includes/footer.php'); ?>	