<?php require('core/init.php'); ?>

<?php
//Obiekt Topic
$topic = new Topic;

//Obiekt User
$user = new User;

//Obiekt Validator
$validate = new Validator;

if(isset($_POST['register'])){
	
	$data = array();
	$data['name'] = $_POST['name'];
	$data['email'] = $_POST['email'];
	$data['username'] = $_POST['username'];
	$data['password'] = md5($_POST['password']);
	$data['password2'] = md5($_POST['password2']);
	$data['about'] = $_POST['about'];
	$data['last_activity'] = date("Y-m-d H:i:s");
	
	//Wymagane pola
	$field_array = array('name','email','username','password','password2');
	
	if($validate->isRequired($field_array)){
		if($validate->isValidEmail($data['email'])){
			if($validate->passwordsMatch($data['password'],$data['password2'])){
					//Dodawanie avataru
					if($user->uploadAvatar()){
						$data['avatar'] = $_FILES["avatar"]["name"];
					}else{
						$data['avatar'] = 'noimage.png';
					}
					//Rejestracja uzytkownika
					if($user->register($data)){
						redirect('index.php', 'Udalo sie zarejestrowac, zapraszamy do logowania', 'success');
					} else {
						redirect('index.php', 'Nie udalo sie zarejestrowac', 'error');
					}
			} else {
				redirect('register.php', 'Twoje haslo nie sa zgodne', 'error');
			}
		} else {
			redirect('register.php', 'Prosze podac poprawny adres email', 'error');
		}
	} else {
		redirect('register.php', 'Prosze wypelnic wymagane pola', 'error');
	}
}

//Obiekt Template
$template = new Template('templates/register.php');



//Wyswietlanie Template
echo $template;