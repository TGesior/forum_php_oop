<?php
session_start();

//Dolaczenie pliku konfiguracyjnego
require_once('config/config.php');

//Funkcje pomocnicze
require_once('helpers/system_helper.php');
require_once('helpers/format_helper.php');
require_once('helpers/db_helper.php');

//Autoloader
function __autoload($class_name){
	require_once('libraries/'.$class_name . '.php');
}