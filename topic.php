<?php require('core/init.php'); ?>

<?php
//Obiekt Topic
$topic = new Topic;

//ID Tematu
$topic_id = $_GET['id'];

//Odpowiedz w temacie
if(isset($_POST['do_reply'])){
	
	$data = array();
	$data['topic_id'] = $_GET['id'];
	$data['body'] = $_POST['body'];
	$data['user_id'] = getUser()['user_id'];

	//Obiekt validator
	$validate = new Validator;
	
	//Wymagane pola
	$field_array = array('body');
	
	if($validate->isRequired($field_array)){
		
		if($topic->reply($data)){
			redirect('topic.php?id='.$topic_id, 'Twoja odpowiedz zostal dodany', 'success');
		} else {
			redirect('topic.php?id='.$topic_id, 'Nie udalo sie dodac odpowiedzi', 'error');
		}
	} else {
		redirect('topic.php?id='.$topic_id, 'Wyplenij wymagane pola!', 'error');
	}
}

//Obiekt Template
$template = new Template('templates/topic.php');


$template->topic = $topic->getTopic($topic_id);
$template->replies = $topic->getReplies($topic_id);
$template->title = $topic->getTopic($topic_id)->title;

//Wyswietlenie Template
echo $template;