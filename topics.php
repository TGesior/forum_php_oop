<?php require('core/init.php'); ?>

<?php
//Obiekt Template
$topic = new Topic;
ini_set("display_errors", "1");

//ID kategorii
$category = isset($_GET['category']) ? $_GET['category'] : null;

//ID uzytkownika
$user_id = isset($_GET['user']) ? $_GET['user'] : null;

//Obiket Template
$template = new Template('templates/topics.php');


if(isset($category)){
	$template->topics = $topic->getByCategory($category);
	$template->title = 'Posty w "'.$topic->getCategory($category)->name.'"';
}


if(isset($user_id)){
	$template->topics = $topic->getByUser($user_id);
	
}


if(!isset($category) && !isset($user_id)){
	$template->topics = $topic->getAllTopics();
}

$template->totalTopics = $topic->getTotalTopics();
$template->totalCategories = $topic->getTotalCategories();

//Wyswietlenie Template
echo $template;