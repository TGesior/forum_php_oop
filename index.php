<?php require('core/init.php'); ?>

<?php
//Obiekt Topic
$topic = new Topic;

//Obiekt User
$user = new User;


//Obiekt Template
$template = new Template('templates/frontpage.php');


$template->topics = $topic->getAllTopics();
$template->totalTopics = $topic->getTotalTopics();
$template->totalCategories = $topic->getTotalCategories();
$template->totalUsers = $user->getTotalUsers();
//Wyswietlenie Template
echo $template;

