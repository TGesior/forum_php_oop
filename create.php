<?php require('core/init.php'); ?>

<?php
//Obiekt Topic
$topic = new Topic;

if(isset($_POST['do_create'])){
	//Obiekt validator
	$validate = new Validator;

	//Tworzenie danych 
	$data = array();
	$data['title'] = $_POST['title'];
	$data['body'] = $_POST['body'];
	$data['category_id'] = $_POST['category'];
	$data['user_id'] = getUser()['user_id'];
	$data['last_activity'] = date("Y-m-d H:i:s");
	
	//Wymagane pola
	$field_array = array('title', 'body', 'category');
	
	if($validate->isRequired($field_array)){
		//Rejestracja uzytkownika
		if($topic->create($data)){
			redirect('index.php', 'Twoj temat zostal dodany', 'success');
		} else {
			redirect('topic.php?id='.$topic_id, 'Nie udalo sie dodac Twojego tematu', 'error');
		}
	} else {
		redirect('create.php', 'Prosze wypelnic wymagane pola', 'error');
	}
}


$template = new Template('templates/create.php');

//Wyswietlenie Template
echo $template;