<?php
/*
 * Formatowanie daty
 */
function formatDate($date){
	$date = date("F j, Y, g:i a",strtotime($date));
	return $date;
}

/*
 * Funkcja zwraca adres URL
 */
function urlFormat($str){
	//Usuwanie spacji ze stringu
	$str = preg_replace('/\s*/', '', $str);
	//Zamiana na male litery
	$str = strtolower($str);
	//URL Encode
	$str = urlencode($str);
	return $str;
}

/*
 * Funkcja ustawia kategorie na aktywna
 */
function is_active($category){
		if(isset($_GET['category'])){
			if($_GET['category'] == $category){
				return 'active';
			} else {
				return '';
			}
		} else {
			if($category == null){
				return 'active';
			}
		}
}